from Exploratory import proba_drinks
from Exploratory import proba_food
from Exploratory import df_cb
import random
import datetime as dt
import numpy as np

################# A EFFACER #############
import os
import pandas as pd

data = os.path.abspath('.\\Data\\Coffeebar_2013-2017.csv')
df_cb = pd.read_csv(data, delimiter=";")

##########################################
a draft of the second part
########### TEST ##########


###########################

print(proba_drinks.ix[4,'DRINKS'])

Drink = ['soda', 'tea', 'water', 'frappucino', 'coffee', 'milkshake']
Food = ['sandwich', 'cookie', 'pie', 'muffin']
PriceDrink = []
PriceFood = []
for i in range (0, 6):
    d = random.randint(1, 5)
    f = random.randint(2, 6)
    if i < 4:
        PriceDrink.append(d)
        PriceFood.append(f)
    else:
        PriceDrink.append(d)
print(PriceDrink, PriceFood)


class OneTimer(object):
    def __init__(self, budget, Typology):
        OneTimer.budget = budget
        OneTimer.Typology = Typology
        OneTimer.achat = []
        OneTimer.price = 0
        OneTimer.pay = 0
        OneTimer.tip = 0

        df_cb['TIME'] = pd.to_datetime (df_cb['TIME'])
        position_heure = random.randint (0, df_cb['TIME'].__len__ ())
        heure2 = df_cb['TIME'].dt.strftime ("%H:%M")
        heure = heure2[position_heure]
        probas_drink_class = proba_drinks[heure] / 100
        h = np.random.choice (np.arange (1, 7),
                              p=probas_drink_class)  # arange(1,7) to select values from Drink variable
        self.achat.append (Drink[h - 1])  # -1 because we need value from 0 to 5 in the j

        self.price = PriceDrink[h-1]

        # deducted the price from the budget

        self.pay = self.budget-self.price
        self.tip = random.randint(1, 10)


    def description(self):
        if self.Typology == "OneTimerTA":
            print("He bought %s at price %s and he has %s Euros left" %(self.achat, self.price, self.pay-self.tip))
        else:
            if self.Typology == "Regular":
                print ("He bought %s at price %s and he has %s Euros left: " % (
                self.achat, self.price, self.pay))

            else:
                print ("Erreur")

    #def dataframe(self):
    #    dataframe =

beatrice = OneTimer(100, "OneTimerTA")
print(beatrice.description())

