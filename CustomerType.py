import random
from ExploratoryDiegoSofian import *

# First, we create a variable that describe the category of food and food and we attibute them prices
cat_of_food = ['Cookie','Muffin','Pie','Sandwich']
food_range = [i for i in range(0,4)]    # Per food, we depict the index
food_price = [2, 3, 3, 5]

cat_of_drink = ['Coffee','Frappucino','Milkshake','Soda','Tea','Water']
drink_range = [i for i in range(0,6)]
drink_price = [3,4,5,3,3,2]

arrival_time = df_cb["TIME"].dt.strftime("%H:%M")

class create_id ():         # A class that store the ID number of customers.
    class_count = 1

class OneTimer(object):
    def __init__(self, budget, typology):
        self.budget = budget
        self.typology = typology
        self.id= 'CID'+ str(create_id.class_count)
        self.habit = [[], [], [], []]

        create_id.class_count += 1
        # We determine randomly an arrival time and the probabilitiy of buying drinks at that time
        time = arrival_time[int(random.uniform(0, arrival_time.__len__()))]
        mean_drink = proba_drinks[time]
        # we  choose randomly the drink given the probability of selling of that time in the bar
        rand_position_drink = int(np.random.choice(drink_range, 1, p=mean_drink))
        buy_drink = cat_of_drink[rand_position_drink]
        self.habit[0].append (time)

        self.habit[2].append(buy_drink) # Drink in the third position of the habit list
        self.budget -= drink_price[rand_position_drink]
        if time <= arrival_time[36]:   # because before 11h, no customer bought food
            buy_food = 'Nothing'
            self.habit[0].append(time) # Arrival time in the 1rst position of the habit list
            #self.habit[3].append(drink_price[rand_position_drink]) # Price paid in the fourth position of the habit list
            #self.habit[3].append(food_price[rand_position_food])
        else:
            mean_food = proba_food[time]
            rand_position_food = int(np.random.choice(len(mean_food), 1, p = mean_food))
            buy_food = cat_of_food[rand_position_food]
            self.habit[1].append(buy_food) # Food in the second position of the habit list
            self.budget -= food_price[rand_position_food]
            self.habit[0].append(time)
            self.habit[3].append(drink_price[rand_position_drink] + food_price[rand_position_food]) # Price paid at fourth position

    def customer(self):
        print("The customer bought %s and %s and paid %s euros " %(self.habit[1], self.habit[2],self.habit[3]))
        if self.typology == "TripAdvisor":
            tip = random.randint (1, 10)
            self.budget -= tip
            print("The %s customer has at the end %s euros" %(self.typology, self.budget))
        else:
            if self.typology == "Regular":
                print("As a %s he has %s euros at the end, and came at the bar at %s"  % (self.typology, self.budget,self.habit[0]))
            else:
                print("Enter either 'TripAdvisor' or'Regular'")
        print("The ID of the Customer is: %s" %self.id)

class Returner(object):
    def __init__(self, typology):
        #self.customerID = id
        self.typology = typology
        self.id = 'CID'+ str(create_id.class_count)
        self.habit = [[], [], [], []]  # datetime, drinks, food, price

        if self.typology == "Hipster":
            budget = 500
        else:
            budget = 250

        #As long as the customer can buy the more expensive drink:
        while budget >= 5:
            if budget > 10:
                time = arrival_time[int(random.uniform(0,arrival_time.__len__()))]
                mean_drink = proba_drinks[time]
                drink_position = int(np.random.choice(drink_range, 1, p = mean_drink))
                buy_drink = cat_of_drink[drink_position]
                self.habit[2].append(buy_drink)
                budget -= drink_price[drink_position]
                self.habit[0].append (time)
                if time <= arrival_time[36]:
                    buy_food = 'Nothing'
                    self.habit[0].append (time)

                else:
                    mean_food = proba_food[time]
                    rand_position_food = int(np.random.choice(len(mean_food), 1, p = mean_food))
                    buy_food = cat_of_food[rand_position_food]
                    self.habit[1].append(buy_food)
                    budget -= food_price[rand_position_food]
                    self.habit[3].append(drink_price[drink_position] + food_price[rand_position_food])
                    self.habit[0].append (time)
            else:
                time = arrival_time[int (random.uniform (0, arrival_time.__len__ ()))]
                mean_drink = proba_drinks[time]
                drink_position = int (np.random.choice (drink_range, 1, p=mean_drink))
                buy_drink = cat_of_drink[drink_position]
                self.habit[0].append (time)
                self.habit[2].append (buy_drink)
                budget -= drink_price[drink_position]
        self.budget = budget


    def customer(self):
        if self.typology == "Hipster":
            print("The Hipster had a budget of 500 €")
            print ("The %s customer bought %s and %s and paid %s. euros " % (self.typology, self.habit[1], self.habit[2], self.habit[3]))
            print ("The %s customer id %s has at the end %s euros and came at the bar at %s" % (self.typology, self.id, self.budget, self.habit[0]))
        else:
            if self.typology == "Regular":
                print ("The Regular consumer had a budget of 250 €")
                print ("The %s customer %s bought %s and %s and paid %s. euros " % (self.typology, self.id, self.habit[1], self.habit[2], self.habit[3]))
                print ("As a %s he has %s euros at the end, and came at the bar at %s" % (self.typology, self.budget, self.habit[0]))
            else:
                 print("Enter either 'Hipster' or'Regular'")
                 print("The ID of the Customer is: %s" % self.id)


CustomerDiego = Returner("Regular")
CustomerSofian = Returner("Hipster")
CustomerSofian2 = OneTimer(100, "Regular")
CustomerDiego2 = OneTimer(100, "TripAdvisor")
print(CustomerDiego.customer())
print(CustomerSofian.customer())
print(CustomerDiego2.customer())
print(CustomerSofian2.customer())
