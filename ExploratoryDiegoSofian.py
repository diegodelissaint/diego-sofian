import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
from numpy import *

#data= os.path.abspath('C:\\Users\\Bertille Miraculeux\\PycharmProjects\\examtsm2018')
#data = os.path.abspath('./Coffeebar_2013-2017.csv')

data = os.path.abspath('.\\Coffeebar_2013-2017.csv')
df_cb = pd.read_csv(data, delimiter=";")
df_cb = df_cb.fillna('Nothing')         # I replace by "Nothing" the missing values
print(df_cb.head(50))

# Q1) What food and drinks are sold by the coffee bar?

food = df_cb["FOOD"]
u_food=food.unique()
print(u_food)
print(len(u_food))

drinks= df_cb["DRINKS"]
u_drinks = drinks.unique()
print(u_drinks)
print(len(u_drinks))

# How many unique customers did the bar have?
cust= df_cb["CUSTOMER"]
print(cust)
u_cust = cust.unique()
print(len(u_cust))

# Q2) Make a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years

df_cb['TIME'] = pd.to_datetime(df_cb['TIME'])

newdata1 = df_cb.groupby(df_cb['TIME'].dt.strftime("%Y %m"))['DRINKS'].count()
newdata1.plot.bar()
plt.xlabel('Time')
plt.ylabel('Drinks')
plt.xticks(rotation=90)
plt.title('The total amount of sold drinks')
plt.tight_layout()
plt.savefig(os.path.abspath('.\\Data\\Drinks.jpg'))

newdata2 = df_cb.groupby(df_cb['TIME'].dt.strftime("%Y %m"))['FOOD'].count()
newdata2.plot.bar()
plt.xlabel('Time')
plt.ylabel('Food')
plt.xticks(rotation=90)
plt.title('The total amount of sold food')
plt.tight_layout()
plt.savefig(os.path.abspath('.\\Data\\Food.jpg'))



df_cb['TIME'] = pd.to_datetime(df_cb['TIME'])

# Probability to buy food at any given time is:
newdata3 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['FOOD'].count()
newdata4 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['FOOD'].value_counts()
proba_food = newdata4/newdata3
print("The probability of buying food at any given time:",round(proba_food*100, 2))
Npath = ".\\Data\\proba_food.csv"
proba_food.to_csv(os.path.join(Npath))

# Probability to buy drinks at any given time is:
newdata5 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['DRINKS'].count()
newdata6 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['DRINKS'].value_counts()
proba_drinks = (newdata6/newdata5)
print("The probability of buying drinks at any given time:",str(round(proba_drinks*100,2)))
Npath1 = ".\\Data\\proba_drinks.csv"
proba_food.to_csv(os.path.join(Npath1))