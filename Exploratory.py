import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
from numpy import *

#data= os.path.abspath('C:\\Users\\Bertille Miraculeux\\PycharmProjects\\examtsm2018')
#data = os.path.abspath('./Coffeebar_2013-2017.csv')

data = os.path.abspath('.\\Data\\Coffeebar_2013-2017.csv')
df_cb = pd.read_csv(data, delimiter=";")
print(df_cb.head())

# What food and drinks are sold by the coffee bar?

food = df_cb["FOOD"]
u_food=food.unique()
print(u_food)
print(len(u_food))

drinks= df_cb["DRINKS"]
u_drinks = drinks.unique()
print(u_drinks)
print(len(u_drinks))

# How many unique customers did the bar have?
cust= df_cb["CUSTOMER"]
print(cust)
u_cust = cust.unique()
print(len(u_cust))

#Make (AT LEAST THESE PLOTS!) : a bar plot of the total amount of sold foods
# (plot1) and drinks (plot2) over the five years

# I convert the timestamp to time format
# I group by month the sells of drinks and foods
# I make my two barplots
# And i save them
df_cb['TIME'] = pd.to_datetime(df_cb['TIME'])
newdata1 = df_cb.groupby(df_cb['TIME'].dt.strftime("%Y %m"))['DRINKS'].count()

newdata1.plot.bar()
plt.xlabel('Time')
plt.ylabel('Drinks')
plt.xticks(rotation=90)
plt.title('The total amount of sold drinks')
plt.tight_layout()
plt.savefig(os.path.abspath('.\\Data\\Drinks.jpg'))
plt.show()

df_cb1 = df_cb.dropna()	        # I clear NaN values in my FOOD column
newdata2 = df_cb1.groupby(df_cb['TIME'].dt.strftime("%Y %m"))['FOOD'].count()
newdata2.plot.bar()
plt.xlabel('Time')
plt.ylabel('Food')
plt.xticks(rotation=90)
plt.title('The total amount of sold food')
plt.tight_layout()
plt.savefig(os.path.abspath('.\\Data\\Food.jpg'))
plt.show()
# Probability to buy food at any given time is:
df_cb1 = df_cb.dropna()	        # I clear NaN values in my Dataframe
df_cb['TIME'] = pd.to_datetime(df_cb['TIME'])
newdata2 = df_cb1.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['FOOD'].count()
newdata3 = df_cb1.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['FOOD'].value_counts()
proba_food= (newdata3/newdata2)*100
print("The probability of buying food at any given time:",round(proba_food, 2))


# Probability to buy drinks at any given time is:
newdata4 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['DRINKS'].count()
newdata5 = df_cb.groupby(df_cb['TIME'].dt.strftime("%H:%M"))['DRINKS'].value_counts()
proba_drinks= (newdata5/newdata4)*100
print("The probability of buying drinks at any given time:",str(round(proba_drinks,2)))



